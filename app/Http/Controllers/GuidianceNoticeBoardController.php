<?php

namespace App\Http\Controllers;

use App\Models\GuidianceNoticeBoard;
use Illuminate\Http\Request;

class GuidianceNoticeBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GuidianceNoticeBoard  $guidianceNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function show(GuidianceNoticeBoard $guidianceNoticeBoard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GuidianceNoticeBoard  $guidianceNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function edit(GuidianceNoticeBoard $guidianceNoticeBoard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GuidianceNoticeBoard  $guidianceNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GuidianceNoticeBoard $guidianceNoticeBoard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GuidianceNoticeBoard  $guidianceNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function destroy(GuidianceNoticeBoard $guidianceNoticeBoard)
    {
        //
    }
}
