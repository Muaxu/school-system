<?php

namespace App\Http\Controllers;

use App\Models\Admitted;
use App\Models\Admission;
use Illuminate\Http\Request;

class AdmittedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dbAppliedAdmission  = Admission::where('is_approved', "2")->with('admission_info')->get();
        return view("pages.admitted.index", ["appliedAdmission" => $dbAppliedAdmission, "use" => "Admitted"]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admitted  $admitted
     * @return \Illuminate\Http\Response
     */
    public function show(Admitted $admitted)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admitted  $admitted
     * @return \Illuminate\Http\Response
     */
    public function edit(Admitted $admitted)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admitted  $admitted
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admitted $admitted)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admitted  $admitted
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admitted $admitted)
    {
        //
    }
}
