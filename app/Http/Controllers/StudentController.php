<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Nin;
use App\Models\Guidiance;
use App\Models\Admission;
use App\Models\Admitted;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Student $student)
    {
        $progressStudent = Student::where("uuid", $student->uuid)->first();
        $studentData = $progressStudent;

        if($studentData)
        {
            $progressStudent = Guidiance::where("student_uuid", $student->uuid)->first();
            
            if($progressStudent)
            {
                $dbAdmissionId = Student::all()->last();
                return view("guidiance.admission.create", ["admissionId" => $dbAdmissionId, "use" => "admission", "studentUuid" => $studentData]);
            }
            else
            {
                $dbAdmissionId = Student::all()->last();
                return view("guidiance.admission.create", ["admissionId" => $dbAdmissionId, "use" => "guidiance", "studentUuid" => $studentData]);
            }
        }
        else
        {
            return "not";
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dbAdmissionId = Student::all()->last();
        return view("guidiance.admission.create", ["admissionId" => $dbAdmissionId, "use" => "student", "studentUuid" => Null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nin_number'        => "required|max:11|min:11|unique:nins",
            'first_name'        => "required|string",
            'surname'           => "required|string",
            'last_name'         => "required|string",
            'gender'            => "required|string",
            'date_of_birth'     => "required|string",
            'state'             => "required|string",
            'lga'               => "required|string",
            'address'           => "required|string",
            'religion'          => "required|string",
            'tribe'             => "required|string",
        ]);

        $ninStatus = Nin::create([
            'uuid'              => Str::orderedUuid(),
            'nin_number'        => $request->nin_number,
            'date_of_birth'     => $request->date_of_birth,
            'gender'            => $request->gender,
            'state'             => $request->state,
            'lga'               => $request->lga,
            'address'           => $request->address,
            'tribe'             => $request->tribe,
            'religion'          => $request->religion,
        ]);

        $status = Student::create([
            'uuid'              => Str::orderedUuid(),
            'first_name'        =>  $request->first_name,
            'surname'           =>  $request->surname,
            'last_name'         =>  $request->last_name,
            'nin_uuid'          =>  $ninStatus->uuid,
        ]);

        return redirect()->route("student", $status->uuid);
        
        if($status){
            return redirect()->back();
        }
            return "not working";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $dbStudent  = Student::where("is_done", "1")->with('student_nin_info',
                                    'student_guidiance_info',
                                    'student_admission_info',
                                    )->get();
        return view("guidiance.admission.index", ["studentData" => $dbStudent]);
        
    }

    public function searchOne(Request $request)
    {
        $searchResult = Admitted::where("admitted_class", $request->class)->where("admitted_classification", $request->admitted_classification)->with("admission_info","admission_info.student_nin_info")->get(); 
        return view("pages.student.index", ["searchResult" => $searchResult]);
    }

    public function searchTwo(Request $request)
    {
        $searchResult = Admitted::where("admitted_class", $request->class)->with("admission_info","admission_info.student_nin_info")->get(); 
        return view("pages.student.index", ["searchResult" => $searchResult]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }


    // public function director(Student $student)
    // {
    //     if(Student::where("uuid", $student)->get())
    //     {
    //         return $student;
    //     }
    // }

    public function progress()
    {
        $dbStudent  = Student::where("is_done", "0")->with('student_nin_info')->get();
        return view("guidiance.admission.progress", ["studentData" => $dbStudent]);
    }
}
