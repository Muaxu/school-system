<?php

namespace App\Http\Controllers;

use App\Models\StudentNoticeBoard;
use Illuminate\Http\Request;

class StudentNoticeBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StudentNoticeBoard  $studentNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function show(StudentNoticeBoard $studentNoticeBoard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StudentNoticeBoard  $studentNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentNoticeBoard $studentNoticeBoard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StudentNoticeBoard  $studentNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentNoticeBoard $studentNoticeBoard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StudentNoticeBoard  $studentNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentNoticeBoard $studentNoticeBoard)
    {
        //
    }
}
