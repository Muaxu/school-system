<?php

namespace App\Http\Controllers;

use App\Models\PaymentList;
use Illuminate\Http\Request;

class PaymentListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaymentList  $paymentList
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentList $paymentList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PaymentList  $paymentList
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentList $paymentList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaymentList  $paymentList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentList $paymentList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaymentList  $paymentList
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentList $paymentList)
    {
        //
    }
}
