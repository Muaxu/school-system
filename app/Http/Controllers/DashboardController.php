<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Guidiance;
use App\Models\Admission;
use App\Models\Student;
use App\Models\Nin;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $dbAdmissionStatus = Admission::all();
        $dbAdmissionStatus = 1;
        $inProgess = count(Student::where("is_done", "0")->get());
        $approved = count(Admission::where("is_approved", "1")->get());
        $rejected = count(Admission::where("is_approved", "2")->get());
        $pending = count(Admission::where("is_approved", "0")->get());
        $totalStudents = count(Student::all());
        $boys = count(Nin::where("gender", "M")->get());
        $girls = count(Nin::where("gender", "F")->get());
        return view("pages.dashboard", ["approved" => $approved, "rejected" => $rejected, "pending" => $pending,
        "inProgess" => $inProgess, "admissionStatus" => $dbAdmissionStatus, "totalStudents" => $totalStudents,
        "boys" => $boys, 'girls' => $girls]);
    }

    public function student_management()
    {
        // $dbAdmissionStatus = Admission::all();
        $dbAdmissionStatus = 1;
        $inProgess = count(Student::where("is_done", "0")->get());
        $approved = count(Admission::where("is_approved", "1")->get());
        $rejected = count(Admission::where("is_approved", "2")->get());
        $pending = count(Admission::where("is_approved", "0")->get());
        $totalStudents = count(Student::all());
        $boys = count(Nin::where("gender", "M")->get());
        $girls = count(Nin::where("gender", "F")->get());
        return view("studentManagement.dashboard", ["approved" => $approved, "rejected" => $rejected, "pending" => $pending,
        "inProgess" => $inProgess, "admissionStatus" => $dbAdmissionStatus, "totalStudents" => $totalStudents,
        "boys" => $boys, 'girls' => $girls]);
    }


    public function guidiance()
    {
        // $dbAdmissionStatus = Admission::all();
        $dbAdmissionStatus = 1;
        $inProgess = count(Student::where("is_done", "0")->get());
        $approved = count(Admission::where("is_approved", "1")->get());
        $rejected = count(Admission::where("is_approved", "2")->get());
        $pending = count(Admission::where("is_approved", "0")->get());
        return view("guidiance.dashboard", ["approved" => $approved, "rejected" => $rejected, "pending" => $pending,
        "inProgess" => $inProgess, "admissionStatus" => $dbAdmissionStatus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
