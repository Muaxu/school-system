<?php

namespace App\Http\Controllers;

use App\Models\SeniorSecondary;
use Illuminate\Http\Request;

class SeniorSecondaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SeniorSecondary  $seniorSecondary
     * @return \Illuminate\Http\Response
     */
    public function show(SeniorSecondary $seniorSecondary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SeniorSecondary  $seniorSecondary
     * @return \Illuminate\Http\Response
     */
    public function edit(SeniorSecondary $seniorSecondary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SeniorSecondary  $seniorSecondary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SeniorSecondary $seniorSecondary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SeniorSecondary  $seniorSecondary
     * @return \Illuminate\Http\Response
     */
    public function destroy(SeniorSecondary $seniorSecondary)
    {
        //
    }
}
