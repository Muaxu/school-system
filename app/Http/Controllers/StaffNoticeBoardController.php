<?php

namespace App\Http\Controllers;

use App\Models\StaffNoticeBoard;
use Illuminate\Http\Request;

class StaffNoticeBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StaffNoticeBoard  $staffNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function show(StaffNoticeBoard $staffNoticeBoard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StaffNoticeBoard  $staffNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function edit(StaffNoticeBoard $staffNoticeBoard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StaffNoticeBoard  $staffNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StaffNoticeBoard $staffNoticeBoard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StaffNoticeBoard  $staffNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaffNoticeBoard $staffNoticeBoard)
    {
        //
    }
}
