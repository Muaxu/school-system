<?php

namespace App\Http\Controllers;

use App\Models\MainNoticeBoard;
use Illuminate\Http\Request;

class MainNoticeBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MainNoticeBoard  $mainNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function show(MainNoticeBoard $mainNoticeBoard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MainNoticeBoard  $mainNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function edit(MainNoticeBoard $mainNoticeBoard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MainNoticeBoard  $mainNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainNoticeBoard $mainNoticeBoard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MainNoticeBoard  $mainNoticeBoard
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainNoticeBoard $mainNoticeBoard)
    {
        //
    }
}
