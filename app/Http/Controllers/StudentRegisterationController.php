<?php

namespace App\Http\Controllers;

use App\Models\StudentRegisteration;
use Illuminate\Http\Request;

class StudentRegisterationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StudentRegisteration  $studentRegisteration
     * @return \Illuminate\Http\Response
     */
    public function show(StudentRegisteration $studentRegisteration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StudentRegisteration  $studentRegisteration
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentRegisteration $studentRegisteration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StudentRegisteration  $studentRegisteration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentRegisteration $studentRegisteration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StudentRegisteration  $studentRegisteration
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentRegisteration $studentRegisteration)
    {
        //
    }
}
