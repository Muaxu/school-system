<?php

namespace App\Http\Controllers;

use App\Models\Guidiance;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class GuidianceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'                          => "required|string",
            'occupation'                    => "required|string",
            'phone_number'                  => "required|min:10|max:14",
            // 'alternative_phone_number'      => "min:10|max:14",
            'relationship_with_student'     => "required|string",
            'email_address'                 => "required|email",
            'tribe'                         => "required|string",
            'religion'                      => "required|string",
            'residencial_state'             => "required|string",
            'residencial_lga'               => "required|string",
            'residencial_address'           => "required|string",
        ]);

        $status = Guidiance::create([
        'uuid'                          => Str::orderedUuid(),
        'student_uuid'                  => $request->studentUuid,
        'name'                          => $request->name, 
        'occupation'                    => $request->occupation,
        'phone_number'                  => $request->phone_number,
        'alternative_phone_number'      => $request->alternative_phone_number,
        'relationship_with_student'     => $request->relationship_with_student,
        'email_address'                 => $request->email_address,
        'tribe'                         => $request->tribe,
        'religion'                      => $request->religion,
        'residencial_state'             => $request->residencial_state,
        'residencial_lga'               => $request->residencial_lga,
        'residencial_address'           => $request->residencial_address,
        ]);

        if($status)
        {
            return redirect()->route('student', $request->studentUuid);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Guidiance  $guidiance
     * @return \Illuminate\Http\Response
     */
    public function show(Guidiance $guidiance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Guidiance  $guidiance
     * @return \Illuminate\Http\Response
     */
    public function edit(Guidiance $guidiance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Guidiance  $guidiance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Guidiance $guidiance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Guidiance  $guidiance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guidiance $guidiance)
    {
        //
    }
}
