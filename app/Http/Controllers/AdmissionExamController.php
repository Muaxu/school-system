<?php

namespace App\Http\Controllers;

use App\Models\AdmissionExam;
use Illuminate\Http\Request;

class AdmissionExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dbAppliedAdmission  = Admission::where('is_approved', "2")->with('admission_info')->get();
        return view("pages.admitted.exam_list", ["appliedAdmission" => $dbAppliedAdmission, "use" => "Rejected"]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AdmissionExam  $admissionExam
     * @return \Illuminate\Http\Response
     */
    public function show(AdmissionExam $admissionExam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AdmissionExam  $admissionExam
     * @return \Illuminate\Http\Response
     */
    public function edit(AdmissionExam $admissionExam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AdmissionExam  $admissionExam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdmissionExam $admissionExam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AdmissionExam  $admissionExam
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdmissionExam $admissionExam)
    {
        //
    }
}
