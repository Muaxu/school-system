<?php

namespace App\Http\Controllers;

use App\Models\Admission;
use App\Models\AdmissionMenu;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Admission $admission)
    {
        if(count(Admission::all()) == "0")
        {
            $admission_id = "School/year/001";
        }
        else
        {
            $admissionId = Admission::all()->last();
            $lastAdmissionId = $admissionId->admission_id;
            $selectId = explode("year/",$lastAdmissionId);
            $admission_id = end($selectId);
            $admission_id = ++$admission_id;
            if($admission_id < 10)
            {
                $admission_id = "School/year/0".$admission_id;
            }
            else
            {
                $admission_id = "School/year/$admission_id";
            }
        }

        $request->validate([
            'applied_class'             => 'required',
            'applied_class_section'     => 'required'
        ]);

        
        $Status = Student::where("uuid", $request->studentUuid)->update([
            'is_done'       =>  "1"
        ]);
        
        if($Status)
        {
            $status = Admission::create([
                'uuid'                      => Str::orderedUuid(),
                'admission_id'              => $admission_id,
                'student_uuid'              => $request->studentUuid,
                'class_applied_for'         => $request->applied_class,
                'applied_class_section'     => $request->applied_class_section,
            ]);
        }

        if($status)
        {
            return redirect()->route('student.show');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admission  $admission
     * @return \Illuminate\Http\Response
     */
    public function show(Admission $admission)
    {
    //     return $admission;
    //     $dbAppliedAdmission  = Admission::with('admission_info')->get();
    //     return view("pages.admission.applied", ["appliedAdmission" => $dbAppliedAdmission, "use" => "applied"]);
    }

    public function all()
    {
        $dbAppliedAdmission  = Admission::with('admission_info')->get();
        return view("pages.admission.applied", ["appliedAdmission" => $dbAppliedAdmission, "use" => "All"]);
    }

    public function pending()
    {
        $dbAppliedAdmission  = Admission::where('is_approved', "0")->with('admission_info')->get();
        return view("pages.admission.applied", ["appliedAdmission" => $dbAppliedAdmission, "use" => "Pending"]);
    }

    public function approved()
    {
        $dbAppliedAdmission  = Admission::where('is_approved', "1")->with('admission_info')->get();
        return view("pages.admission.applied", ["appliedAdmission" => $dbAppliedAdmission, "use" => "Approved"]);
    }

    public function rejected()
    {
        $dbAppliedAdmission  = Admission::where('is_approved', "2")->with('admission_info')->get();
        return view("pages.admission.applied", ["appliedAdmission" => $dbAppliedAdmission, "use" => "Rejected"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admission  $admission
     * @return \Illuminate\Http\Response
     */
    public function edit(Admission $admission)
    {
        //
    }

    public function applying($applied_section)
    {
        if($applied_section == "Nusery")
        {
            $dbNusery = array("Nur. 1", "Nur. 2", "Nur. 3");
            $applied = " ";
            $applied .= '<option value="" hidden>Select Class</option>';

            foreach($dbNusery as $fetchNusery)
            {
                $applied .= "<option value='$fetchNusery'>$fetchNusery</option>";
            }
            return $applied;
        }
        elseif($applied_section == "Primary")
        {
            $dbPrimary = array("Pri. 1", "Pri. 2", "Pri. 3", "Pri. 4", "Pri. 5", "Pri. 6");
            $applied = " ";
            $applied .= '<option value="" hidden>Select Class</option>';

            foreach($dbPrimary as $fetchPrimary)
            {
                $applied .= "<option value='$fetchPrimary'>$fetchPrimary</option>";
            }
            return $applied;
        }
        elseif($applied_section == "Junior Secondary")
        {
            $dbJS = array("Jss. 1", "Jss. 2", "Jss. 3");
            $applied = " ";
            $applied .= '<option value="" hidden>Select Class</option>';

            foreach($dbJS as $fetchJS)
            {
                $applied .= "<option value='$fetchJS'>$fetchJS</option>";
            }
            return $applied;
        }
        elseif($applied_section == "Senior Secondary")
        {
            $dbSS = array("SSS. 1", "SSS. 2", "SSS. 3");
            $applied = " ";
            $applied .= '<option value="" hidden>Select Class</option>';

            foreach($dbSS as $fetchSS)
            {
                $applied .= "<option value='$fetchSS'>$fetchSS</option>";
            }
            return $applied;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admission  $admission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admission $admission)
    {
        Admission::where("uuid", $admission->uuid)->update([
            "is_approved"   => $request->status
        ]);

        return redirect()->back();
    }

    public function setup()
    {
        $dbAdmissionMenu = AdmissionMenu::all();
        return view("pages.admission.setup", ["setupPageTitle" => "Admission", "admissionMenu" => $dbAdmissionMenu]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admission  $admission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admission $admission)
    {
        //
    }
}
