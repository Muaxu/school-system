<?php

namespace App\Http\Controllers;

use App\Models\SetUp;
use Illuminate\Http\Request;

class SetUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SetUp  $setUp
     * @return \Illuminate\Http\Response
     */
    public function show(SetUp $setUp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SetUp  $setUp
     * @return \Illuminate\Http\Response
     */
    public function edit(SetUp $setUp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SetUp  $setUp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SetUp $setUp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SetUp  $setUp
     * @return \Illuminate\Http\Response
     */
    public function destroy(SetUp $setUp)
    {
        //
    }
}
