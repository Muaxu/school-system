<?php

namespace App\Http\Controllers;

use App\Models\JuniorSecondary;
use Illuminate\Http\Request;

class JuniorSecondaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JuniorSecondary  $juniorSecondary
     * @return \Illuminate\Http\Response
     */
    public function show(JuniorSecondary $juniorSecondary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JuniorSecondary  $juniorSecondary
     * @return \Illuminate\Http\Response
     */
    public function edit(JuniorSecondary $juniorSecondary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JuniorSecondary  $juniorSecondary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JuniorSecondary $juniorSecondary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JuniorSecondary  $juniorSecondary
     * @return \Illuminate\Http\Response
     */
    public function destroy(JuniorSecondary $juniorSecondary)
    {
        //
    }
}
