<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'uuid',
        'first_name',
        'surname',
        'last_name',
        'nin_uuid',
        'is_done'
    ];

    public function student_nin_info()
    {
        return $this->belongsTo(Nin::class, 'nin_uuid', 'uuid');
    }

    public function student_guidiance_info()
    {
        return $this->belongsTo(Guidiance::class, 'guidiance_uuid', 'uuid');
    }

    public function student_admission_info()
    {
        return $this->hasOne(Admission::class, 'student_uuid', 'uuid');
    }
}
