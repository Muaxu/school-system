<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guidiance extends Model
{
    use HasFactory;
    use softDeletes;

    protected $fillable = [
        'uuid',
        'student_uuid',
        'name',
        'occupation',
        'phone_number',
        'alternative_phone_number',
        'relationship_with_student',
        'email_address',
        'tribe',
        'religion',
        'residencial_state',
        'residencial_lga',
        'residencial_address',
    ];

    public function guidiance_info()
    {
        return $this->belongsTo(Student::class, 'student_uuid', 'uuid');
    }
}
