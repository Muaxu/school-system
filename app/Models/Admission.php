<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admission extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'admission_id',
        'student_uuid',
        'class_applied_for',
        'applied_class_section',
        'applied_class_classification',
        'admitted_class',
        'admitted_section',
        'admitted_classification',
    ];
    // public function resolveRouteBinding($value, $field = null)
    // { 
    //     return $this->where($field ?? 'uuid', $value)->firstOrFail();
    // }

    public function admission_info()
    {
        return $this->belongsTo(Student::class, 'student_uuid', 'uuid');
    }
}
