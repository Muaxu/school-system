<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nin extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'uuid',
        'nin_number',
        'date_of_birth',
        'gender',
        'state',
        'lga',
        'address',
        'tribe',
        'religion',
    ];

    public function nin_info()
    {
        return $this->hasOne(Student::class, 'nin_uuid', 'uuid');
    }
}
