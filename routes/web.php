<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdmittedController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\GuidianceController;
use App\Http\Controllers\AdmissionController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\NoticeBoardController;
use App\Http\Controllers\AdmissionExamController;
use App\Http\Controllers\AdmissionMenuController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/create-student', function () {
//     return view('students.student.create');
// });

Route::get("/dashboard", [DashboardController::class, "index"])->name("dashboard");

//Admin Admission
Route::get("/all-admission", [AdmissionController::class, 'all'])->name("admin.all.admission");
Route::get("/pending-admission", [AdmissionController::class, 'pending'])->name("admin.pending.admission");
Route::get("/approved-admission", [AdmissionController::class, 'approved'])->name("admin.approved.admission");
Route::get("/rejected-admission", [AdmissionController::class, 'rejected'])->name("admin.rejected.admission");
Route::get("/admitted-admission", [AdmittedController::class, 'index'])->name("admin.admitted.admission");
Route::get("/admission-exam-list", [AdmissionExamController::class, 'index'])->name("exam.admission");
Route::get("/setup-admission", [AdmissionController::class, 'setup'])->name("admin.setup.admission");
Route::put("/update-admission/{admission:uuid}", [AdmissionController::class, 'update'])->name("admin.update.admission");
Route::put("/update-setup-admission/{admissionMenu:uuid}", [AdmissionMenuController::class, 'update'])->name("admin.update.setup.admission");
// Route::get("/applied-admission", [AdmissionController::class, 'show'])->name("admin.applied.admission");
//End of Admin Admission

Route::get("/main-notice-board", [NoticeBoardController::class, "main"])->name("main.notice.board");
Route::get("/staff-notice-board", [NoticeBoardController::class, "staff"])->name("staff.notice.board");
Route::get("/guidiance-notice-board", [NoticeBoardController::class, "guidiance"])->name("guidiance.notice.board");
Route::get("/student-notice-board", [NoticeBoardController::class, "student"])->name("student.notice.board");

// Student
Route::post("/senior-search-student", [StudentController::class, "searchOne"])->name("searchOne.student");
Route::post("/search-student", [StudentController::class, "searchTwo"])->name("searchTwo.student");
// Route::get("/search-result-student", [StudentController::class, "search"])->name("search.student");
// End of Student

Route::get("/student-management-dashboard", [DashboardController::class, "student_management"])->name("student.management.dashboard");
Route::get("/student-management-primary-section", [DashboardController::class, "student_management"])->name("student.management.primary.section");
Route::view("/student-management-section", "studentManagement.section.index")->name("student.management.section");


// Route::group(["prefix" => "student", "namespace" => "student"], function(){
    Route::get("/guidiance-dashboard", [DashboardController::class, "guidiance"])->name("guidiance.dashboard");
    Route::get("/list-student", [StudentController::class, "show"])->name("student.show");
    Route::get("/create-student", [StudentController::class, "create"])->name("student.create");
    Route::post("/store-student", [StudentController::class, "store"])->name("student.store");
    Route::post("/store-guidiance", [GuidianceController::class, "store"])->name("guidiance.store");
    Route::post("/store-admission", [AdmissionController::class, "store"])->name("admission.store");
    Route::get("/student/{student:uuid}", [StudentController::class, "index"])->name("student");
    Route::get("/progress-student", [StudentController::class, "progress"])->name("student.progress");
    Route::get("/applied-section/{applied_section}", [AdmissionController::class, "applying"])->name("student.applying");
// });
