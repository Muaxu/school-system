<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Nin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Nin::factory(10)->create();
    }
}
