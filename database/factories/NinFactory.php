<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\Nin;
use Illuminate\Database\Eloquent\Factories\Factory;


class NinFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
            return [
                'uuid' =>   Str::orderedUuid(),
                'nin_number' =>   Str::random(10),
                'date_of_birth' => $this->faker->date($format = 'Y-m-d'),
                'gender' => $this->randomElement(['M', 'F']),
                'state' => $this->randomElement(['Gombe', 'Kaduna', 'Jos', 'Kano', 'Abuja', 'Lagos', 'Bauchi', 'Katsina', 'Sokoto', 'Jigawa', 'Zamfara']),
                'lga' => $this->randomElement(['Akko', 'Gombe', 'Dukko', 'Nafada']),
                'address'  =>  $this->randomElement(['Hotel', 'Office', 'Police Station', 'Hospital', 'School']),
                'tribe'    =>  $this->randomElement(['Hausa', 'Ebira', 'Yoruba', 'Fulani']),
                'religion'  =>  $this->randomElement(['Muslim','Chiristian'])
        ];
    }
}
