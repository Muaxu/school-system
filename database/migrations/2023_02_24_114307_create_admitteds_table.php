<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmittedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admitteds', function (Blueprint $table) {
            $table->id();
            $table->string("uuid", 100)->unique();
            $table->string("student_uuid", 100);
            $table->string("admission_uuid", 100);
            $table->string("admission_exam_uuid", 100);
            $table->string("admitted_class", 20)->default(Null)->nullable();
            $table->string("admitted_section", 20)->default(Null)->nullable();
            $table->string("admitted_classification", 20)->default(Null)->nullable();
            $table->enum("is_active", [0,1])->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('admission_uuid')->references('uuid')->on('admissions');
            // $table->foreign('admission_exam_uuid')->references('uuid')->on('admission_exams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admitteds');
    }
}
