<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admissions', function (Blueprint $table) {
            $table->id();
            $table->string("uuid", 100)->unique();
            $table->string("admission_id", 20)->unique();
            $table->string("student_uuid", 100);
            $table->string("class_applied_for", 20);
            $table->string("applied_class_section", 20);
            $table->string("applied_class_classification", 20)->default(Null)->nullable();
            $table->enum("is_approved", [0,1,2])->default(0); //2-rejected, 1-approved, 0-pending
            $table->enum("is_active", [0,1])->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('student_uuid')->references('uuid')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admissions');
    }
}
