<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->uuid("uuid", 100)->unique();
            $table->string("first_name", 50);
            $table->string("surname", 50);
            $table->string("last_name", 50);
            $table->string("nin_uuid", 100)->unique();
            $table->enum("is_done", [0,1])->default(0);
            $table->enum("is_active", [0,1])->default(1);
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('nin_uuid')->references('uuid')->on('nins');
            // $table->foreign("nin_uuid")->references("uuid")->on("nins");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
