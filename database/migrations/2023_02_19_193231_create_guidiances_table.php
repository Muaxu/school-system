<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuidiancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guidiances', function (Blueprint $table) {
            $table->id();
            $table->string("uuid", 100)->unique();
            $table->string("student_uuid", 100);
            $table->string("name", 50);
            $table->string("occupation", 50);
            $table->string("phone_number", 15);
            $table->string("alternative_phone_number", 15)->nullable();
            $table->string("relationship_with_student", 30);
            $table->string("email_address", 50);
            $table->string("tribe", 50);
            $table->string("religion", 50);
            $table->string("residencial_state", 50);
            $table->string("residencial_lga", 50);
            $table->string("residencial_address", 100);
            $table->enum("is_active", [0,1])->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('student_uuid')->references('uuid')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guidiances');
    }
}
