<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNurseriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nurseries', function (Blueprint $table) {
            $table->id();
            $table->string("student_reg_uuid", 100)->unique();
            $table->enum("is_active", [0,1])->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('student_reg_uuid')->references('uuid')->on('student_registerations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nurseries');
    }
}
