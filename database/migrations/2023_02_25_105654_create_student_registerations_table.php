<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentRegisterationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_registerations', function (Blueprint $table) {
            $table->id();
            $table->string("uuid", 100)->unique();
            $table->string('student_uuid', 100)->unique();
            $table->string("reg_year", 20);
            $table->string("reg_no", 30)->unique();
            $table->enum("is_active", [0,1])->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('student_uuid')->references('uuid')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_registerations');
    }
}
