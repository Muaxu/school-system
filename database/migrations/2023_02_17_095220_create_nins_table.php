<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nins', function (Blueprint $table) {
            $table->id();
            $table->string("uuid", 100)->unique();
            $table->string("nin_number", 11)->unique();
            $table->date("date_of_birth");
            $table->string("gender", 10);
            $table->string("state", 50);
            $table->string("lga", 50);
            $table->string("address", 100);
            $table->string("tribe", 30);
            $table->string("religion", 20);
            $table->enum("is_active", [0,1])->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nins');
    }
}
