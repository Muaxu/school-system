@extends("navigations.admin.nav")
@section("workspace")
    <div class="row mb-4">
        <div class="col">
            <h3>
                {{$use }} Admission
            </h3>
            <small style="color:rgba(0, 0, 0, 0.218)">
                Admission > {{$use }}
            </small>
        </div>
        <div class="col nav" style="display:inline-flex">
            <div class="navbar-nav" style="margin-left:auto">
                <button class="btn btn-outline-dark rounded-circle" onclick="printPage()">#</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table" id="here">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Admission ID</th>
                        <th>Name</th>
                        <th>Applied For</th>
                        <th>Admiited Into</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($appliedAdmission as $fetchAppliedAdmission => $fetchAppliedAdmissionData)
                        <tr>
                            <td>{{ ++$fetchAppliedAdmission }}</td>
                            <td>{{ $fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->admission_id : " "}}</td>
                            <td>{{ ($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->admission_info->first_name : " "). " " .($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->admission_info->surname : " "). " ".($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->admission_info->last_name : " ")}}</td>
                            <td>{{ ($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->applied_class_section : "") . " ( ".($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->class_applied_for : " "). " )"}}</td>
                            <td>{{ ($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->applied_class_section : "") . " ( ".($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->class_applied_for : " "). " )"}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style>
        .w-5{
            display:none;
        }
    </style>
    <script>
        function printPage()
        {
            print.$("#here");
            
        }
    </script>
@endsection