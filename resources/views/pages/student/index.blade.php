@extends("navigations.admin.nav")
@section("workspace")
    <div class="row">
        <h4>
           
        </h4>
    </div>
    <div class="row mt-4">
        <div class="col">
        <table class="table ">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Admission No</th>
                        <th>Name</th>
                        <th>Class</th>
                        <th>Status</th>
                        <th colspan="2">Actions</th>
                        <!-- <th></th> -->
                    </tr>
                </thead>
                <tbody>
                    @foreach($searchResult as $fetchStudent => $fetchSearchResult)
                        <tr>
                            <td>{{ ++$fetchStudent }}</td>
                            <td>{{ $fetchSearchResult ? $fetchSearchResult->admission_id : "" }}</td>
                            <td>{{ ($fetchSearchResult->admission_info ? $fetchSearchResult->admission_info->first_name : ""). " " .($fetchSearchResult->admission_info ? $fetchSearchResult->admission_info->surname : ""). " " . (($fetchSearchResult->admission_info ? $fetchSearchResult->admission_info->last_name : ""))}}</td>
                            <td>{{ ($fetchSearchResult ? $fetchSearchResult->admitted_class : "") . ($fetchSearchResult ? $fetchSearchResult->admitted_classification : "")}}</td>
                            <td>
                                <button class="btn btn-outline-dark rounded-circle">#</button>
                                <button class="btn btn-outline-dark rounded-circle">#</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @if(count($searchResult) > "0")
                <span class="text-success">{{ count($searchResult)}} records found</span>
            @else
                <span class="text-danger">{{ count($searchResult)}} records found</span>
            @endif
        </div>
    </div>

@endsection