@extends("navigations.admin.nav")
@section("workspace")
    <div class="row mb-3">
        <div class="col">
            <h3>
                {{$use }} Admission
            </h3>
            <small style="color:rgba(0, 0, 0, 0.218)">
                Admission > {{$use }}
            </small>
        </div>
        <div class="col nav" style="display:inline-flex">
            <div class="navbar-nav" style="margin-left:auto">
                <button class="btn btn-outline-dark rounded-circle" onclick="printPage()">#</button>
            </div>
        </div>
    </div>
    <div class="mb-4" style="width:28vmax;float:right;margin-left:auto">
        <!-- <div class="col-4 float-right"> -->
            <input type="search" style="float:right" placeholder="Search..." class="form-control">
        <!-- </div> -->
    </div>
    <div class="row">
        <div class="col">
            <table class="table" id="here">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Admission ID</th>
                        <th>Name</th>
                        <th>Applied For</th>
                        <th>Status</th>
                        @if($use == "Pending")
                            <th colspan="2">Actions</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($appliedAdmission as $fetchAppliedAdmission => $fetchAppliedAdmissionData)
                        <tr>
                            <td>{{ ++$fetchAppliedAdmission }}</td>
                            <td>{{ $fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->admission_id : " "}}</td>
                            <td>{{ ($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->admission_info->first_name : " "). " " .($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->admission_info->surname : " "). " ".($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->admission_info->last_name : " ")}}</td>
                            <td>{{ ($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->applied_class_section : "") . " ( ".($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->class_applied_for : " "). " )"}}</td>
                            <td>
                                <span class="btn-outline-info rounded-circle">
                                    @if(($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->is_approved : " ")  == "0")
                                        <span class="text-info">Pending</span>
                                    @elseif(($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->is_approved : " ")  == "1")
                                        <span class="text-success">Approved</span>
                                    @elseif(($fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->is_approved : " ")  == "2")
                                        <span class="text-danger">Rejected</span>
                                    @endif
                                </span>
                            </td>
                            @if($use == "Pending")
                                <td>
                                    <button class="btn btn-outline-dark rounded-circle">#</button>
                                        <form action="{{ Route('admin.update.admission', $fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->uuid : '') }}" method="post" style="display:inline">
                                        @csrf
                                        @method("put")
                                        <input type="hidden" name="status" value="1">
                                        <button class="btn btn-outline-dark rounded-circle">
                                        </form> #
                                    </button>
                                        <form action="{{ Route('admin.update.admission', $fetchAppliedAdmissionData ? $fetchAppliedAdmissionData->uuid : '') }}" method="post" style="display:inline">
                                        @csrf
                                        @method("put")
                                        <input type="hidden" name="status" value="2">
                                        <button class="btn btn-outline-dark rounded-circle">
                                        </form> #
                                    </button>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style>
        .w-5{
            display:none;
        }
    </style>
    <script>
        function printPage()
        {
            print.$("#here");
            
        }
    </script>
@endsection