@extends("navigations.admin.nav")
@section("workspace")

    <div class="row">
        <h4>
            SetUp
        </h4>
    </div>
    <div class="row mt-5">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    {{$setupPageTitle}} Menu
                </div>
                    <ul class="mt-2">
                        @foreach($admissionMenu as $fetchAdmissionMenu)
                            <li class="mt-1">{{ $fetchAdmissionMenu ? $fetchAdmissionMenu->menu_name : " " }}
                                <!-- <form action="{{ Route('admin.update.setup.admission', ($fetchAdmissionMenu ? $fetchAdmissionMenu->uuid : ' ')) }}" method="post" style="display:inline"> -->
                                    <!-- @csrf -->
                                    <!-- @method("put") -->
                                    <button data-bs-toggle="collapse" data-bs-target="#memu{{$fetchAdmissionMenu ? $fetchAdmissionMenu->uuid : ' ' }}" onclick=" menu('{{$fetchAdmissionMenu->uuid}}')" style="float:right" class="btn btn-outline-dark rounded-circle pt-0 pb-0">#</button>  
                                <!-- </form> -->
                            </li>
                        @endforeach
                    </ul>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-header">
                    
                </div>
                @foreach($admissionMenu as $fetchAdmissionMenu)
                    <div class="collapse" id="memu{{$fetchAdmissionMenu ? $fetchAdmissionMenu->uuid : ' ' }}">
                        <h1>{{ $fetchAdmissionMenu ? $fetchAdmissionMenu->uuid : ' ' }}</h1>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-header">
                    
                </div>
            </div>
        </div>
    </div>
    <script>
        function menu(uuid)
        {
            $("#menu" + uuid).click();
        }

    </script>
@endsection