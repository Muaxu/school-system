@extends("navigations.studentManagement.nav")
@section("workspace")

    <div class="row">
        <div class="col-8">
            <div class="row">
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            Admission
                        </div>
                        <div class="container-fluid">
                            <ul class="mt-2">
                                <li class="text-success">Approved
                                    <span class="badge bg-success" style="float:right">{{$approved}}</span>
                                </li>
                                <li class="text-danger">Rejected
                                    <span class="badge bg-danger" style="float:right">{{$rejected}}</span>
                                </li>
                                <li class="text-info">Pending
                                    <span class="badge bg-info" style="float:right">{{$pending}}</span>
                                </li>
                        </ul>
                        </div>
                    </div>                     
                </div>
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            Calender
                        </div>
                        <div class="container-fluid mt-1">
                            <!-- <span class="badge badge-lg bg-dark mx-auto text-center"> -->
                            <h2 class="text-center">
                                <?php echo Date("d") ?>
                            </h2>
                            <p>hh</p>
                            <!-- </span> -->
                        </div>
                    </div>
                </div>
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            Item
                        </div>
                    </div>
                </div>
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            item
                        </div>
                        <div class="container-fluid">
                        </div>
                    </div>                     
                </div>
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            Staffs
                        </div>
                        <div class="p-1">
                            <ul class="">
                                <li class="" style="color:darkslategrey">Total
                                    <span class="badge" style="float:right;background-color:darkslategrey">0</span>
                                </li>
                                <li class="text-secondary">Academic
                                    <span class="badge bg-secondary" style="float:right">0</span>
                                </li>
                                <li class="text-secondary">Non-academic
                                    <span class="badge bg-secondary" style="float:right">0</span>
                                </li>
                        </ul>
                        </div>
                    </div>                     
                </div>
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            Students
                        </div>
                        <div class="container-fluid">
                            <ul class="mt-2">
                                <li class="" style="color:darkslategrey">Total
                                    <span class="badge" style="float:right;background-color:darkslategrey">{{$totalStudents}}</span>
                                </li>
                                <li class="text-secondary">Boys
                                    <span class="badge bg-secondary" style="float:right">{{$boys}}</span>
                                </li>
                                <li class="text-secondary">Girls
                                    <span class="badge bg-secondary" style="float:right">{{$girls}}</span>
                                </li>
                        </ul>
                        </div>
                    </div>                     
                </div>
            </div>
        </div>
        <div class="col-4 mt-3">
            <div class="col">
                <div class="card">
                    <div class="card-header text-center">
                        Notice Board
                    </div>
                    <br><br>
                    <br><br>
                    <br><br><br>
                    <br><br><br>
                </div>
            </div>
        </div>
    </div>
    <span class="mt-4"></span>
    <hr>
    <div class="row">
        <div class="col-3 mt-2">
            <div class="row">
                <div class="col-12 mt-1">
                    <div class="card-footer">
                        <div class="text-center">
                            Admission | @if($admissionStatus == "1") <span class="text-success">Open</span> @else <span class="text-danger">Closed</span> @endif <br> <span class="text-danger">( until 00:00:00)</span>
                        </div>
                    </div> 
                </div>
            </div>  
        </div>
        <div class="col-3 mt-2">
            <div class="row">
                <div class="col-12 mt-1">
                    <div class="card-footer">
                        <div class="text-center">
                            <span>Today <br>
                                <?php echo Date("D-M-Y") ?>
                            </span>
                        </div>
                    </div> 
                </div>
            </div>  
        </div>
        <div class="col-3 mt-2">
            <div class="row">
                <div class="col-12 mt-1">
                    <div class="card-footer">
                        <div class="text-center">
                            <span>Today <br>
                                <?php echo Date("D-M-Y") ?>
                            </span>
                        </div>
                    </div> 
                </div>
            </div>  
        </div>
        <div class="col-3 mt-2">
            <div class="row">
                <div class="col-12 mt-1">
                    <div class="card-footer">
                        <div class="text-center">
                            <span>Today <br>
                                <?php echo Date("D-M-Y") ?>
                            </span>
                        </div>
                    </div> 
                </div>
            </div>  
        </div>
    </div>

@endsection