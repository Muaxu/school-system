@extends("navigations.guidiance.nav")
@section("workspace")
    <div class="row">
        <div class="col">
            <table class="table ">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nin Number</th>
                        <th>Name</th>
                        <th>Applied For</th>
                        <th>Status</th>
                        <th colspan="2">Actions</th>
                        <!-- <th></th> -->
                    </tr>
                </thead>
                <tbody>
                    @foreach($studentData as $fetchStudent => $fetchStudentData)
                        <tr>
                            <td>{{ ++$fetchStudent }}</td>
                            <td>{{ $fetchStudentData->student_nin_info ? $fetchStudentData->student_nin_info->nin_number : " "}}</td>
                            <td>{{ ($fetchStudentData ? $fetchStudentData->first_name : " "). " " .($fetchStudentData ? $fetchStudentData->surname : " "). " ".($fetchStudentData ? $fetchStudentData->last_name : " ")}}</td>
                            <td>{{ ($fetchStudentData->student_admission_info ? $fetchStudentData->student_admission_info->applied_class_section : " "). "( ".($fetchStudentData->student_admission_info ? $fetchStudentData->student_admission_info->class_applied_for : " "). ")"}}</td>
                            <td>
                                @if(($fetchStudentData->student_admission_info ? $fetchStudentData->student_admission_info->is_approved : " ")  == "0")
                                    {{"Pending"}}
                                @elseif(($fetchStudentData->student_admission_info ? $fetchStudentData->student_admission_info->is_approved : " ")  == "1")
                                    {{"Approved"}}
                                @elseif(($fetchStudentData->student_admission_info ? $fetchStudentData->student_admission_info->is_approved : " ")  == "2")
                                    {{"Rejected"}}
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-outline-dark rounded-circle">#</button>
                                <button class="btn btn-outline-dark rounded-circle">#</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style>
        .w-5{
            display:none;
        }
    </style>
@endsection