@extends("navigations..guidiance.nav")
@section("workspace")
    <div class="row">
        <div class="col">
            <table class="table ">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nin Number</th>
                        <th>Name</th>
                        <th class="text-danger">Application Status</th>
                        <th colspan="2">Actions</th>
                        <!-- <th></th> -->
                    </tr>
                </thead>
                <tbody>
                    @foreach($studentData as $fetchStudent => $fetchStudentData)
                        <tr>
                            <td>{{ ++$fetchStudent }}</td>
                            <td>{{ $fetchStudentData->student_nin_info ? $fetchStudentData->student_nin_info->nin_number : " "}}</td>
                            <td>{{ ($fetchStudentData ? $fetchStudentData->first_name : " "). " " .($fetchStudentData ? $fetchStudentData->surname : " "). " ".($fetchStudentData ? $fetchStudentData->last_name : " ")}}</td>
                            <td class="text-danger" id="expireStatus{{$fetchStudentData->uuid}}"></td>
                            <td>
                                  <button type="button" id="click" class="btn btn-outline-dark rounded-circle" onclick='getUuid("{{$fetchStudentData->uuid}}")'>#</button>
                                <a href="{{ Route('student', $fetchStudentData ? $fetchStudentData->uuid : '') }}" class="btn btn-outline-dark rounded-circle">#</button>
                            </td>
                        </tr>
                        <script>
                            setInterval(function(){$(":button").click()}, 1000);

                            function maketimer(uuid)
                            {
                                var endtime = new Date("2023-02-23 17:27:05 GMT+01:00")
                                endtime = (Date.parse(endtime) / 1000);
                                var now = new Date();
                                now = (Date.parse(now) / 1000);
                                var timeleft = endtime - now;
                                var days = Math.floor(timeleft / 86400);
                                var hours = Math.floor((timeleft - (days * 86400)) / 3600 );
                                var minutes = Math.floor((timeleft - (days * 86400) - (hours * 3600)) / 60);
                                var seconds = Math.floor((timeleft - (days * 86400) - (hours * 3600) - (minutes * 60)));
                                if((hours == "0" && minutes == "0" && seconds == "0") || endtime < now)
                                {
                                    $("#expireStatus"+ uuid).html("Admission Expired");
                                    stop();
                                }
                                if(hours < "10")
                                {
                                    hours = "0" + hours;
                                }
                                if(minutes < "10")
                                {
                                    minutes = "0" + minutes
                                }

                                // $("#days")html
                                $("#expireStatus" + uuid).html("expires in" + " " + hours + ":" + minutes + ":" + seconds);

                            }

                            function getUuid(uuid)
                            {
                                setInterval(function(){maketimer(uuid);}, 1000);
                            }
                            function stop()
                            {
                                clearInterval(interval);
                            }
                        </script>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style>
        .w-5{
            display:none;
        }
    </style>

   
@endsection