@extends("navigations.guidiance.nav")
@section("workspace")

    
    <nav class="nav">
        <button class="btn @if($use != 'student'){{'btn-outline-dark'}} @endif" data-bs-toggle="collapse" data-bs-target="#info-1" @if($use != 'student'){{'disabled'}} @endif>
            <small>Student's <br> Information</small>
        </button>
        <button class="btn @if($use != 'guidiance'){{'btn-outline-dark'}} @endif" data-bs-toggle="collapse" data-bs-target="#info-2" @if($use != 'guidiance'){{'disabled'}} @endif>
            <small >Guidiance <br> Information</small>
        </button>
        <button class="btn @if($use != 'admission'){{'btn-outline-dark'}} @endif" data-bs-toggle="collapse" data-bs-target="#info-3" @if($use != 'admission'){{'disabled'}} @endif>
            <small >Admission <br> Information</small>
        </button>
        <button class="btn @if($use != 'school'){{'btn-outline-dark'}} @endif" data-bs-toggle="collapse" data-bs-target="#info-4" @if($use != 'school'){{'disabled'}} @endif>
            <small >School <br> Infomation</small>
        </button>
        <p class="text-dark" style="margin-left:auto;">Admission Id: {{$admissionId ? ++$admissionId->id : "1"}}</p>
    </nav>
    <div class="mt-5">
        <div class="row">
            <div class="col collapse @if($use == 'student'){{'show'}} @endif" id="info-1">
                <form action="{{ Route('student.store') }}" method="post">
                    @csrf

                    <div class="row">
                        @component("components.input", 
                        [
                        "type" => "number",
                        "name" => "nin_number",
                        "placeholder" => "NIN Number",
                        "value" => "old"
                        ])
                        @endcomponent
                        
                        @component("components.input", 
                        [
                        "type" => "type",
                        "name" => "first_name",
                        "placeholder" => "First Name",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "surname",
                        "placeholder" => "Surname",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "last_name",
                        "placeholder" => "Last Name",
                        "value" => "old"
                        ])
                        @endcomponent

                        <x-select>
                            <x-slot name="placeholder">Gender</x-slot>
                            <x-slot name="name">gender</x-slot>
                            <option value="M" @if(old('gender') == "M"){{ "selected" }} @endif>Male</option>
                            <option value="F" @if(old('gender') == "F"){{ "selected" }} @endif>Female</option>
                        </x-select>

                        @component("components.input", 
                        [
                        "type" => "date",
                        "name" => "date_of_birth",
                        "placeholder" => "Date Of Birth",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "address",
                        "placeholder" => "Address",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "state",
                        "placeholder" => "State",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "lga",
                        "placeholder" => "L.G.A",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "tribe",
                        "placeholder" => "Tribe",
                        "value" => "old"
                        ])
                        @endcomponent

                        <x-select>
                            <x-slot name="placeholder">Religion</x-slot>
                            <x-slot name="name">religion</x-slot>
                            <option value="Muslim" @if(old('religion') == "Muslim"){{ "selected" }} @endif>Muslim</option>
                            <option value="Chirstian" @if(old('religion') == "Chirstian"){{ "selected" }} @endif>Chirstian</option>
                        </x-select>
                    </div>
                    <div style="float:right" class="row mt-3">
                        <button type="submit" class="btn float-right btn-outline-dark">Submit</button>
                    </div>
                </form>
            </div>
            <div class="col collapse @if($use == 'guidiance'){{'show'}} @endif" id="info-2">
                <form action="{{ Route('guidiance.store') }}" method="post">
                    @csrf

                    <input type="hidden" name="studentUuid" value="{{ $studentUuid ? $studentUuid->uuid : ' '}}">
                    <div class="row">
                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "name",
                        "placeholder" => "Guidiance Name",
                        "value" => "old"
                        ])
                        @endcomponent
                        
                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "occupation",
                        "placeholder" => "Occupation",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "tel",
                        "name" => "phone_number",
                        "placeholder" => "Phone Number",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "tel",
                        "name" => "alternative_phone_number",
                        "placeholder" => "Alternative Phone Number",
                        "value" => "old"
                        ])
                        @endcomponent

                        <x-select>
                            <x-slot name="placeholder">Relationship With Student</x-slot>
                            <x-slot name="name">relationship_with_student</x-slot>
                            <option value="Son" @if(old("relationship_with_student") == "Son"){{ "selected" }} @endif>Son</option>
                            <option value="Daughter" @if(old("relationship_with_student") == "Daughter"){{ "selected" }} @endif>Daughter</option>
                            <option value="Others" @if(old("relationship_with_student") == "Others"){{ "selected" }} @endif>Others</option>
                        </x-select>

                        @component("components.input", 
                        [
                        "type" => "email",
                        "name" => "email_address",
                        "placeholder" => "Email Address",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "tribe",
                        "placeholder" => "Tribe",
                        "value" => "old"
                        ])
                        @endcomponent

                        <x-select>
                            <x-slot name="placeholder">Religion</x-slot>
                            <x-slot name="name">religion</x-slot>
                            <option value="Muslim" @if(old("religion") == "Muslim"){{ "selected" }} @endif>Muslim</option>
                            <option value="Chirstian" @if(old("religion") == "Chirstian"){{ "selected" }} @endif>Chirstian</option>
                        </x-select>

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "residencial_state",
                        "placeholder" => "Residencial State",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "residencial_lga",
                        "placeholder" => "Residencial LGA",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "residencial_address",
                        "placeholder" => "Residencial Address",
                        "value" => "old"
                        ])
                        @endcomponent
                    </div>
                    <div style="float:right" class="row mt-3">
                        <button type="submit" class="btn float-right btn-outline-dark">Submit</button>
                    </div>
                </form>
            </div>
            <div class="col collapse @if($use == 'admission'){{'show'}} @endif" id="info-3">
                <p>I <u>{{($studentUuid ? $studentUuid->first_name : ' '). " " .($studentUuid ? $studentUuid->surname : ' '). " " .($studentUuid ? $studentUuid->last_name : ' ')}}</u> agree to ....</p>
                <form action="{{ Route('admission.store') }}" method="post">
                    @csrf
                    <input type="hidden" name="studentUuid" value="{{ $studentUuid ? $studentUuid->uuid : ' '}}">
                    <div class="row">
                        <div class="col-4 mt-4">
                            <div class="form-group">
                                <label for="">Class Section</label>
                                <select onchange='applied($("#appliedClass").val())' id="appliedClass" name="applied_class_section" class='form-control @error("applied_class_section") is-invalid @enderror'>
                                    <option value="" hidden>Class Section</option>
                                    <option value="Nusery" @if(old("applied_class_section") == "Nusery"){{ "selected" }} @endif>Nusery</option>
                                    <option value="Primary" @if(old("applied_class_section") == "Primary"){{ "selected" }} @endif>Primary</option>
                                    <option value="Junior Secondary" @if(old("applied_class_section") == "Junior Secondary"){{ "selected" }} @endif>Junior Secondary</option>
                                    <option value="Senior Secondary" @if(old("applied_class_section") == "Senior Secondary"){{ "selected" }} @endif>Senior Secondary</option>
                                </select>
                                @error("applied_class_section")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-4 mt-4">
                            <div class="form-group">
                                <label for="">Class</label>
                                <select name="applied_class" class='form-control @error("applied_class") is-invalid @enderror' id="here">
                                </select>
                                @error("applied_class_section")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <!-- @error("applied_class_section") -->
                        <!-- <span class="invalid-feedback">{{ $message }}</span> -->
                    <!-- @enderror -->
                    </div>
                    <div style="float:right">
                        <button type="submit" class="btn float-right btn-outline-dark">Agree & Process</button>
                    </div>
                    </div>
                </form>
            </div>
            <div class="col collapse @if($use == 'school'){{'show'}} @endif" id="info-4">
                <form action="{{ Route('student.store') }}" method="post">
                    @csrf

                    <div class="row">
                        @component("components.input", 
                        [
                        "type" => "number",
                        "name" => "nin_number",
                        "placeholder" => "NIN Number",
                        "value" => "old"
                        ])
                        @endcomponent
                        
                        @component("components.input", 
                        [
                        "type" => "type",
                        "name" => "first_name",
                        "placeholder" => "First Name",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "surname",
                        "placeholder" => "Surname",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "last_name",
                        "placeholder" => "Last Name",
                        "value" => "old"
                        ])
                        @endcomponent

                        <x-select>
                            <x-slot name="placeholder">Gender</x-slot>
                            <x-slot name="name"></x-slot>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </x-select>

                        @component("components.input", 
                        [
                        "type" => "date",
                        "name" => "date_of_birth",
                        "placeholder" => "",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "tel",
                        "name" => "phone_number",
                        "placeholder" => "Phone Number",
                        "value" => "old"
                        ])
                        @endcomponent

                        @component("components.input", 
                        [
                        "type" => "text",
                        "name" => "state",
                        "placeholder" => "State",
                        "value" => "old"
                        ])
                        @endcomponent
                    </div>
                    <div style="float:right">
                        <button type="submit" class="btn float-right btn-outline-dark">Save & Continue</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- @if($use == "student")
        <script>
/applied-section
        </script>
    @elseif($use == "guidiance")

    @endif -->
    <script>
        function applied(res)
        {   
            $.ajax({
                type: "get",
                url: "/applied-section/" + res,
                success: function (response) {
                    $("#here").html(response);
                }
            });
        }
    </script>
    

@endsection