@extends("navigations.guidiance.nav")
@section("workspace")

    <div class="row">
        <div class="col-8">
            <div class="row">
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            Admission
                        </div>
                        <div class="container-fluid">
                            <ul class="mt-2">
                                <li class="text-success">Approved
                                    <span class="badge bg-success" style="float:right">{{$approved}}</span>
                                </li>
                                <li class="text-danger">Rejected
                                    <span class="badge bg-danger" style="float:right">{{$rejected}}</span>
                                </li>
                                <li class="text-info">Pending
                                    <span class="badge bg-info" style="float:right">{{$pending}}</span>
                                </li>
                        </ul>
                        </div>
                    </div>                     
                </div>
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            Item
                        </div>
                    </div>
                </div>
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            Item
                        </div>
                    </div>
                </div>
                <div class="col-4 mt-2">
                    <div class="row">
                        <div class="col-12">
                            <div class="card-footer">
                                <span class="text-warning"> Admission In Progress</span>
                                <div class="pt-1 text-warning">
                                    <span class="badge bg-warning">{{$inProgess}}</span> In Progress
                                </div>
                                <div class="pt-1 text-danger">
                                    <span class="badge bg-danger">{{$rejected}}</span> expires in 00:00:00
                                </div>
                            </div> 
                        </div>
                        <div class="col-12 mt-2">
                            <div class="card-footer">
                                <div class="text-center">
                                    Admission | @if($admissionStatus == "1") <span class="text-success">Open</span> @else <span class="text-danger">Closed</span> @endif <br> <span class="text-danger">( until 00:00:00)</span>
                                </div>
                            </div> 
                        </div>
                    </div>  
                    
                </div>
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            Item
                        </div>
                    </div>
                </div>
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="card-header text-center">
                            Item
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4 mt-3">
            <div class="col">
                <div class="card">
                    <div class="card-header text-center">
                        Notice Board
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection