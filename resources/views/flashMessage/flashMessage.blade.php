@if($message = Session::get("success"))
    <span class="alert alert-success">{{$message}}</span>
@elseif($message = Session::get("failed"))
    <span class="alert alert-danger">{{$message}}</span>
@else

@endif