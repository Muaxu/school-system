<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <title>Document</title>
</head>
<body>
    <nav class="nav bg-dark navbar-expands-lg ">
        <a href="" class="navbar-brand p-3 text-light">School System <strong>|</strong><small> Guidiance</small></a>
        <ul class="navbar nav p-3" style="margin-left:auto">
           <!-- <li class="navbar-items">Home</li> -->
           <!-- <li class="navbar-items">c</li> -->
        </ul>
    </nav>
    <div class="row" style="position:relative">
            @include('navigations.guidiance.sideNav')
        <div class="col-10 mt-2">
            <div class="container-fluid">
                <div class="card  p-5">
                    @yield("workspace")
                </div>
            </div>
        </div>
        </div>
</body>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

</html>