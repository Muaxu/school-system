<div class="col-2">
    <nav class="bg-primary">
        <ul class="list-group nav">
            <a href="{{Route('guidiance.dashboard')}}" class="dropdown-item list-group-item p-4">
                Dashboard
            </a>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#admission" data-bs-toggle="collapse">
                Admission
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#payment" data-bs-toggle="collapse">
                Payment
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#school_fees" data-bs-toggle="collapse">
                Option
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#" data-bs-toggle="collapse">
                Option
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#" data-bs-toggle="collapse">
                Option
            </li>
        </ul>
    </nav>
</div>

<!-- Admission -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="admission">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.843);">
        <div class="btn-group-vertical nav">
            <a href="{{ Route('student.create') }}" class="btn" style="text-align:left">
                New Admission
            </a>
            <a href="{{ Route('student.show') }}" class="btn" style="text-align:left">
                Applied
            </a>
            <a href="{{ Route('student.progress') }}" class="btn" style="text-align:left">
                In Progress
            </a>
        </div>
    </nav>
</div>
<!-- End of Admission -->

<!-- Payment -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="payment">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.843);">
        <div class="btn-group-vertical nav">
            <a href="{{ Route('student.create') }}" class="btn" style="text-align:left">
                School Fees
            </a>
            <a href="{{ Route('student.show') }}" class="btn" style="text-align:left">
                Lesson
            </a>
            <a href="{{ Route('student.progress') }}" class="btn" style="text-align:left">
                Others
            </a>
        </div>
    </nav>
</div>
<!-- End of payment -->


<script>
    // $("#admission").is("collapse", function(){
        // alert("working");
    // })
</script>