<div class="col-2">
    <nav class="" style="height:87vmin;overflow-y:auto">
        <ul class="list-group nav">
            <a href="{{Route('dashboard')}}" class="dropdown-item list-group-item p-4">
                Dashboard
            </a>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#admission" data-bs-toggle="collapse">
                Admission
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#payment" data-bs-toggle="collapse">
                Payment
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#notice_board" data-bs-toggle="collapse">
                Notice Board
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#students" data-bs-toggle="collapse">
                Students
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#activities" data-bs-toggle="collapse">
                Activities
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#control" data-bs-toggle="collapse">
                Controls
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#setUp" data-bs-toggle="collapse">
                SetUp
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#security" data-bs-toggle="collapse">
                Secuirty
            </li>
            
        </ul>
    </nav>
</div>

<!-- Admission -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="admission">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.956);">
        <div class="btn-group-vertical nav">
            <a href="{{Route('admin.all.admission')}}" class="btn" style="text-align:left">
                All
            </a>
            <a href="{{Route('admin.pending.admission')}}" class="btn" style="text-align:left">
                Pending
            </a>
            <a href="{{Route('admin.approved.admission')}}" class="btn" style="text-align:left">
                Approved
            </a>
            <a href="{{Route('admin.rejected.admission')}}" class="btn" style="text-align:left">
                Rejected
            </a>
            <div class="line mt-3 mb-3" style="width:20vmax">

            </div>
            <a href="{{Route('admin.admitted.admission')}}" class="btn" style="text-align:left">
                Admitted
            </a>
            <li class="btn" data-bs-target="#admitted_exam"  style="text-align:left" data-bs-toggle="modal">
                Exams Record
            </li>
            <a href="{{Route('admin.setup.admission')}}" class="btn" style="text-align:left">
                SetUp
            </a>
        </div>
    </nav>
</div>
<!--  -->
<div class="modal mt-5 pt-3" id="admitted_exam">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: rgba(85, 85, 85, 0.66);">
            <div class="modal-header">
                <h3>Admission Exams</h3>
            </div>
            <div class="modal-body">
                <form action="{{ Route('searchTwo.student') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-6 mt-4">
                            <div class="form-group">
                                <label for="">Select Year</label>
                                <select name="class" class='form-control @error("class") is-invalid @enderror'>
                                    <option value="" hidden>Select Year</option>
                                    @for($year = 2010; $year <= 2024; $year++)
                                        <option value="{{$year}}">{{ $year }}</option>
                                    @endfor                 
                                </select>
                                @error("category")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6 mt-4">
                            <div class="form-group">
                                <label for="">Select Section</label>
                                <select name="section" class='form-control @error("section") is-invalid @enderror'>
                                    <option value="" hidden>Select Section</option>
                                    <option value="First Term">First Term</option>
                                    <option value="Second Term">Second Term</option>
                                    <option value="Third Term">Third Term</option>
                                </select>
                                @error("category")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" class="mt-5 btn btn-dark">Proceed</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
<!-- End of Admission -->

<!-- Payment -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="payment">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.956);">
        <div class="btn-group-vertical nav">
            <a href="" class="btn" style="text-align:left">
                School Fees
            </a>
            <a href="" class="btn" style="text-align:left">
                Lesson
            </a>
            <a href="" class="btn" style="text-align:left">
                Others
            </a>
        </div>
    </nav>
</div>
<!-- End of payment -->

<!-- notice board -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="notice_board">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.956);">
        <div class="btn-group-vertical nav">
            <a href="{{ Route('main.notice.board') }}" class="btn" style="text-align:left">
                Main Board
            </a>
            <a href="{{ Route('staff.notice.board')}}" class="btn" style="text-align:left">
                Staff's Board
            </a>
            <a href="{{ Route('guidiance.notice.board')}}" class="btn" style="text-align:left">
                Guidiance Board
            </a>
            <a href="{{ Route('student.notice.board') }}" class="btn" style="text-align:left">
                Student's Board
            </a>
        </div>
    </nav>
</div>
<!-- End of notice board -->

<!-- students -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="students">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.956);">
        <div class="btn-group-vertical nav">
            <a href="" data-bs-toggle="modal" data-bs-target="#nursery" class="btn" style="text-align:left">
                Nursery
            </a>
            <a href="" data-bs-toggle="modal" data-bs-target="#" class="btn" style="text-align:left">
                Staff's Board
            </a>
            <a href="" data-bs-toggle="modal" data-bs-target="#" class="btn" style="text-align:left">
                Guidiance Board
            </a>
            <a href="" data-bs-toggle="modal" data-bs-target="#senior_secondary" class="btn" style="text-align:left">
                Senior Secondary
            </a>
        </div>
    </nav>
</div>
<div class="modal mt-5 pt-3" id="nursery">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: rgba(85, 85, 85, 0.66);">
            <div class="modal-header"></div>
            <div class="modal-body">
                <form action="{{ Route('searchTwo.student') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-6 mt-4">
                            <div class="form-group">
                                <label for="">Section</label>
                                    <input class="form-control" value="Nursery" readonly>
                            </div>
                        </div>
                        <div class="col-6 mt-4">
                            <div class="form-group">
                                <label for="">Class</label>
                                <select name="class" class='form-control @error("class") is-invalid @enderror'>
                                    <option value="" hidden>Select Class</option>
                                    <option value="Nur. 1">Nur. 1</option>
                                    <option value="Nur. 2">Nur. 2</option>
                                    <option value="Nur. 3">Nur. 3</option>
                                </select>
                                @error("category")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" class="mt-5 btn btn-dark">Proceed</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal mt-5 pt-3" id="senior_secondary">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: rgba(85, 85, 85, 0.66);">
            <div class="modal-header"></div>
            <div class="modal-body">
                <form action="{{ Route('searchOne.student') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-6 mt-4">
                            <div class="form-group">
                                <label for="">Category</label>
                                <select name="admitted_classification" class='form-control @error("admitted_classification") is-invalid @enderror'>
                                    <option value="" hidden>Select Category</option>
                                    <option value="Science">Science</option>
                                    <option value="Art">Art</option>
                                    <option value="Commerce">Commerce</option>
                                </select>
                                @error("admitted_classification")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6 mt-4">
                            <div class="form-group">
                                <label for="">Class</label>
                                <select name="admitted_class" class='form-control @error("admitted_class") is-invalid @enderror'>
                                    <option value="" hidden>Select Class</option>
                                    <option value="SSS. 1">SSS. 1</option>
                                    <option value="SSS. 2">SSS. 2</option>
                                    <option value="SSS. 3">SSS. 3</option>
                                </select>
                                @error("category")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" class="mt-5 btn btn-dark">Proceed</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal mt-5 pt-3" id="senior_secondary">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: rgba(85, 85, 85, 0.66);">
            <div class="modal-header"></div>
            <div class="modal-body">
                <form action="" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-6 mt-4">
                            <div class="form-group">
                                <label for="">Category</label>
                                <select name="admitted_classification" class='form-control @error("admitted_classification") is-invalid @enderror'>
                                    <option value="" hidden>Select Category</option>
                                    <option value="Science">Science</option>
                                    <option value="Art">Art</option>
                                    <option value="Commerce">Commerce</option>
                                </select>
                                @error("admitted_classification")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6 mt-4">
                            <div class="form-group">
                                <label for="">Class</label>
                                <select name="admitted_class" class='form-control @error("admitted_class") is-invalid @enderror'>
                                    <option value="" hidden>Select Class</option>
                                    <option value="SSS. 1">SSS. 1</option>
                                    <option value="SSS. 2">SSS. 2</option>
                                    <option value="SSS. 3">SSS. 3</option>
                                </select>
                                @error("category")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" class="mt-5 btn btn-dark">Proceed</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal mt-5 pt-3" id="senior_secondary">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: rgba(85, 85, 85, 0.66);">
            <div class="modal-header"></div>
            <div class="modal-body">
                <form action="" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-6 mt-4">
                            <div class="form-group">
                                <label for="">Category</label>
                                <select name="admitted_classification" class='form-control @error("admitted_classification") is-invalid @enderror'>
                                    <option value="" hidden>Select Category</option>
                                    <option value="Science">Science</option>
                                    <option value="Art">Art</option>
                                    <option value="Commerce">Commerce</option>
                                </select>
                                @error("admitted_classification")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-6 mt-4">
                            <div class="form-group">
                                <label for="">Class</label>
                                <select name="admitted_class" class='form-control @error("admitted_class") is-invalid @enderror'>
                                    <option value="" hidden>Select Class</option>
                                    <option value="SSS. 1">SSS. 1</option>
                                    <option value="SSS. 2">SSS. 2</option>
                                    <option value="SSS. 3">SSS. 3</option>
                                </select>
                                @error("category")
                                    <span class='invalid-feedback'>{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" class="mt-5 btn btn-dark">Proceed</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End of students -->


<!-- activities -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="activities">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.956);">
        <div class="btn-group-vertical nav">
        <a href="" class="btn" style="text-align:left">
                School Calender
            </a>
            <a href="" class="btn" style="text-align:left">
                Duty Roster 
            </a>
        </div>
    </nav>
</div>
<!-- End of activities -->

<!-- control -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="control">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.956);">
        <div class="btn-group-vertical nav">
        <a href="{{ Route('main.notice.board') }}" class="btn" style="text-align:left">
                Access Control
            </a>
            <a href="{{ Route('staff.notice.board')}}" class="btn" style="text-align:left">
                Permission Control
            </a>
            <a href="{{ Route('guidiance.notice.board')}}" class="btn" style="text-align:left">
                 control
            </a>
            <a href="{{ Route('student.notice.board') }}" class="btn" style="text-align:left">
                Student's Control
            </a>
        </div>
    </nav>
</div>
<!-- End of control -->

<!-- setUp -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="setUp">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.956);">
        <div class="btn-group-vertical nav">
        <a href="{{ Route('main.notice.board') }}" class="btn" style="text-align:left">
                Main SetUp
            </a>
            <a href="{{ Route('staff.notice.board')}}" class="btn" style="text-align:left">
                Staff's SetUp
            </a>
            <a href="{{ Route('guidiance.notice.board')}}" class="btn" style="text-align:left">
                Guidiance SetUp
            </a>
            <a href="{{ Route('student.notice.board') }}" class="btn" style="text-align:left">
                Student's SetUp
            </a>
        </div>
    </nav>
</div>
<!-- End of setUp -->

<!-- security -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="security">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.956);">
        <div class="btn-group-vertical nav">
            <a href="" class="btn" style="text-align:left">
                Secuirty
            </a>
        </div>
    </nav>
</div>
<!-- End of security -->


<script>
    // $("#admission").is("collapse", function(){
        // alert("working");
    // })
</script>