<div class="col-2">
    <nav class="" style="height:87vmin;overflow-y:auto">
        <ul class="list-group nav">
            <a href="{{Route('student.management.dashboard')}}" class="dropdown-item list-group-item p-4">
                Dashboard
            </a>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#admission" data-bs-toggle="collapse">
                Admission
            </li>
            <a href="{{Route('student.management.section')}}" class="dropdown-item list-group-item p-4">
                Sections
            </a>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#control" data-bs-toggle="collapse">
                Tests (C.A)
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#setUp" data-bs-toggle="collapse">
                Exams
            </li>
            <li class="dropdown-item list-group-item p-4" data-bs-target="#security" data-bs-toggle="collapse">
                Secuirty
            </li>
            
        </ul>
    </nav>
</div>

<!-- Admission -->
<div class="col-2 collapse" style="position:absolute;left:14.9%;z-index:1" id="admission">
    <nav class="" style="padding-bottom:56vmin;background-color: rgba(207, 207, 181, 0.843);">
        <div class="btn-group-vertical nav">
            <a href="" class="btn" style="text-align:left">
                All
            </a>
            <a href="" class="btn" style="text-align:left">
                Pending
            </a>
            <a href="" class="btn" style="text-align:left">
                Approved
            </a>
            <a href="" class="btn" style="text-align:left">
                Rejected
            <a href="" class="btn" style="text-align:left">
                SetUp
            </a>
        </div>
    </nav>
</div>
<!-- End of Admission -->

