<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <title>Document</title>
</head>
<body>
    <nav class="nav bg-dark navbar-expands-lg ">
        <a href="" class="navbar-brand p-3 text-light">School System <strong>|</strong><small> Student Management</small></a>
        <ul class="navbar nav p-3" style="margin-left:auto">
           <li class="navbar-items">
                <i class="bg-light p-3" style="border-radius:100%">#</i>
           </li>
           <li class="navbar-items">
                <i class="bg-light p-3" style="border-radius:100%">#</i>
           </li>
        </ul>
    </nav>
    <div class="row" style="position:relative;margin-right:0.2vmin">
            @include('navigations.studentManagement.sideNav')
        <div class="col-10" style="height:88vmin;overflow-y:auto">
            <div class="container-fluid mt-3">
                @include("flashMessage.flashMessage")
                <div class="card p-5">
                    @yield("workspace")
                </div>
            </div>
            <!-- <footer class="" style="position:sticky;margin-bottom:auto"> -->
                <div class="mt-5">
                    <hr>
                    &copy;copyright@briatekComputers. All Rights Reserved
                </div>
            <!-- </footer> -->
        </div>
        </div>
</body>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

</html>