<div class="col-4 mt-4">
    <div class="form-group">
        <label for="">{{$placeholder}}</label>
        <select name="{{$name}}" class='form-control @error("$name") is-invalid @enderror'>
            <option value="" hidden>{{$placeholder}}</option>
            {{$slot}}
        </select>
        @error("$name")
            <span class='invalid-feedback'>{{ $message }}</span>
        @enderror
    </div>
</div>