<div class="col-4 mt-4">
    <div class="form-group">
        <label for="">{{$placeholder}}</label>
        <input type="{{$type}}" name="{{$name}}" placeholder="{{$placeholder}}" class='form-control @error("$name") is-invalid @enderror' value="{{ old($name) }}">
        @error("$name")
            <span class='invalid-feedback'>{{ $message }}</span>
        @enderror
    </div>
</div>